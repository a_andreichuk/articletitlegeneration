from pathlib import Path
from transformers import BertTokenizerFast

DATADIR = Path(".") / "input"
TRAIN_CSV = DATADIR / "train.csv"
TEST_CSV = DATADIR / "test.csv"

TRAIN_TOKENIZED = DATADIR / "train.pkl"
TEST_TOKENIZED = DATADIR / "test.pkl"


MAX_SEQ_LEN = 512
SOS = "[SOS]"
EOS = "[EOS]"

TOKENIZER_DIR = DATADIR / "tokenizer"
TOKENIZER_VOCAB = DATADIR / "tokenizer_vocab.txt"
TOKENIZER = BertTokenizerFast.from_pretrained(str(TOKENIZER_DIR))
