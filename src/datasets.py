import numpy as np
import torch
from torch.utils.data import Dataset
import config


class AbstractTitleDataset(Dataset):
    def __init__(self, abstracts, titles=None):
        if titles is not None:
            if len(abstracts) != len(titles):
                raise ValueError(
                    f"Different lengths of abstracts ({len(abstracts)}) and titles ({(titles)})"
                )
        self.abstracts = abstracts
        self.titles = titles

    def __len__(self):
        return len(self.abstracts)

    @staticmethod
    def select_tokens(tokens, max_num, is_train):
        if len(tokens) <= max_num:
            return tokens
        if is_train:
            num_remove = len(tokens) - max_num
            remove_start = np.random.randint(0, len(tokens) - num_remove - 1)
            return tokens[:remove_start] + tokens[remove_start + num_remove :]
        else:
            return tokens[: max_num // 2] + tokens[-(max_num - max_num // 2) :]

    def __getitem__(self, index):
        abstract = self.abstracts[index]
        abstract = self.select_tokens(
            abstract, max_num=config.MAX_SEQ_LEN, is_train=self.titles is not None
        )

        if self.titles is None:
            return abstract

        title = self.titles[index]
        return abstract, title


class SequenceCollator:
    def __init__(self, pad_token_id: int, ignore_token: int, is_train: bool = True):
        self.pad_token_id = pad_token_id
        self.ignore_token = ignore_token
        self.is_train = is_train

    def __call__(self, batch):
        batch_size = len(batch)

        if self.is_train:
            sequences, labels = list(zip(*batch))
        else:
            sequences, labels = batch, None

        max_seq_len = max(len(seq) for seq in sequences)
        seq_batch = np.zeros(shape=(batch_size, max_seq_len), dtype=int)

        if labels is not None:
            max_target_len = max(len(target) for target in labels)
            labels_batch = np.zeros(shape=(batch_size, max_target_len), dtype=int)
        else:
            labels_batch = None

        for i in range(batch_size):
            s = sequences[i]
            seq_batch[i, : len(s)] = s
            if labels is not None:
                l = labels[i]
                labels_batch[i, : len(l)] = l

        seq_batch = torch.from_numpy(seq_batch)
        if labels_batch is None:
            return seq_batch

        labels_batch = torch.from_numpy(labels_batch)
        return seq_batch, labels_batch
