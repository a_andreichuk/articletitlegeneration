import os
import sys
import shutil
from pathlib import Path

from tqdm import tqdm
import numpy as np
import pandas as ps
from sklearn.model_selection import train_test_split

import torch
from torch.utils.data import DataLoader, Subset

import config
from models import BERT2LSTM
from batteries import (
    seed_all,
    CheckpointManager,
    make_checkpoint,
    t2d,
    TensorboardLogger,
)
from datasets import AbstractTitleDataset, SequenceCollator


IGNORE_TOKEN = -1
BATCH_SIZE = 4


def get_datasets(pickled_data: str) -> tuple:
    df = ps.read_pickle(pickled_data)

    train_df, valid_df = train_test_split(df, test_size=0.1, random_state=2020)

    collator = SequenceCollator(config.TOKENIZER.pad_token_id, IGNORE_TOKEN)

    trainset = AbstractTitleDataset(
        abstracts=train_df["abstract_tokens"].values,
        titles=train_df["title_tokens"].values,
    )
    print(f"Train set shapes - {len(trainset)}")
    # trainset = Subset(trainset, indices=list(range(BATCH_SIZE * 4)))

    train = DataLoader(
        trainset, batch_size=BATCH_SIZE, collate_fn=collator, shuffle=True
    )
    print(f"Batches in train set - {len(train)}")

    validset = AbstractTitleDataset(
        abstracts=valid_df["abstract_tokens"].values,
        titles=valid_df["title_tokens"].values,
    )
    print(f"Validation set shapes - {len(validset)}")
    # validset = Subset(validset, indices=list(range(BATCH_SIZE * 4)))

    valid = DataLoader(validset, batch_size=BATCH_SIZE, collate_fn=collator)
    print(f"Batches in validation set - {len(valid)}")

    return train, valid


def batch_accuracy(true: torch.Tensor, pred: torch.Tensor) -> float:
    acc = (true == pred)[true != IGNORE_TOKEN].float().mean().item()
    return acc


def train_fn(
    model,
    loader,
    device,
    loss_fn,
    optimizer,
    scheduler=None,
    accum_steps: int = 1,
    verbose=True,
):
    model.train()

    losses = []
    accs = []
    with tqdm(total=len(loader), file=sys.stdout, desc="train") as progress:
        for _idx, (bx, by) in enumerate(loader):
            bx, by = t2d((bx, by), device)

            optimizer.zero_grad()

            outputs = model(bx, by)
            outputs = outputs.view(-1, outputs.size(2))
            by = by.view(-1)

            loss = loss_fn(outputs, by)
            _loss = loss.item()
            losses.append(_loss)
            loss.backward()

            _batch_acc = batch_accuracy(by.detach(), outputs.detach().argmax(1))
            accs.append(_batch_acc)

            progress.update(1)
            progress.set_postfix_str(f"loss {_loss:.4f}, acc - {_batch_acc:.4f}")

            if (_idx + 1) % accum_steps == 0:
                optimizer.step()
                if scheduler is not None:
                    scheduler.step()

    return np.mean(losses), np.mean(accs)


def valid_fn(model, loader, device, loss_fn):
    model.eval()

    losses = []
    accs = []
    with torch.no_grad(), tqdm(
        total=len(loader), file=sys.stdout, desc="valid"
    ) as progress:
        for _idx, (bx, by) in enumerate(loader):
            bx, by = t2d((bx, by), device)

            outputs = model(bx, by)
            outputs = outputs.view(-1, outputs.size(2))
            by = by.view(-1)

            loss = loss_fn(outputs, by)
            _loss = loss.item()
            losses.append(_loss)

            _batch_acc = batch_accuracy(by.detach(), outputs.detach().argmax(1))
            accs.append(_batch_acc)

            progress.update(1)
            progress.set_postfix_str(f"loss {_loss:.4f}, acc - {_batch_acc:.4f}")

    return np.mean(losses), np.mean(accs)


def experiment(logdir: Path):
    if os.path.isdir(str(logdir)):
        shutil.rmtree(str(logdir), ignore_errors=True)
        print(f" * Removed '{logdir}'")

    tb_logdir = logdir / "tensorboard"
    device = torch.device("cuda:0")

    seed_all()

    model = BERT2LSTM(len(config.TOKENIZER)).to(device)
    criterion = torch.nn.CrossEntropyLoss(ignore_index=IGNORE_TOKEN)
    optimizer = torch.optim.AdamW(
        [
            {"params": model.bert.parameters(), "lr": 0},
            {"params": model.decoder.parameters()},
        ],
        lr=1e-3,
    )

    train_loader, valid_loader = get_datasets(str(config.TRAIN_TOKENIZED))

    with TensorboardLogger(tb_logdir) as tb:
        stage = "stage0"

        checkpointer = CheckpointManager(
            logdir=logdir / stage,
            metric="batch_accuracy",
            metric_minimization=False,
            save_n_best=2,
        )
        n_epochs = 5
        for epoch in range(1, n_epochs + 1):
            print()
            print(f"[Epoch {epoch}/{n_epochs}]")

            train_loss, train_acc = train_fn(
                model, train_loader, device, criterion, optimizer
            )
            print(f"    train loss - {train_loss:.5f}")
            print(f"train accuracy - {train_acc:.5f}", flush=True)
            print()

            tb.metric(f"{stage}/loss", {"train": train_loss}, epoch)
            tb.metric(
                f"{stage}/accuracy", {"train": train_acc}, epoch,
            )

            valid_loss, valid_acc = valid_fn(model, valid_loader, device, criterion)
            print(f"    valid loss - {valid_loss:.5f}")
            print(f"valid accuracy - {valid_acc:.5f}")
            print()

            tb.metric(f"{stage}/loss", {"valid": valid_loss}, epoch)
            tb.metric(
                f"{stage}/accuracy", {"valid": valid_acc}, epoch,
            )

            epoch_metrics = {
                "train_loss": train_loss,
                "train_accuracy": train_acc,
                "valid_loss": valid_loss,
                "valid_accuracy": valid_acc,
            }

            checkpointer.process(
                metric_value=valid_acc,
                epoch=epoch,
                checkpoint=make_checkpoint(
                    stage, epoch, model, optimizer, metrics=epoch_metrics,
                ),
            )


if __name__ == "__main__":
    logdir = Path(".") / "logs" / "bert"
    experiment(logdir)
