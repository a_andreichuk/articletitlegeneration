import random

# installed
import torch
import torch.nn as nn
import torch.nn.functional as F
from transformers import BertConfig, BertModel

# local files
import config


class Attention(nn.Module):
    def __init__(self, enc_hid_dim, dec_hid_dim):
        super().__init__()

        self.attn = nn.Linear(enc_hid_dim + dec_hid_dim, dec_hid_dim)
        self.v = nn.Linear(dec_hid_dim, 1, bias=False)

    def forward(self, dec_hidden, encoder_outputs):
        batch_size = encoder_outputs.shape[0]
        src_len = encoder_outputs.shape[1]

        dec_hidden = dec_hidden.unsqueeze(1).repeat(1, src_len, 1)

        energy = torch.tanh(self.attn(torch.cat((dec_hidden, encoder_outputs), dim=2)))

        attention = self.v(energy).squeeze(2)

        return F.softmax(attention, dim=1)


class Decoder(nn.Module):
    def __init__(
        self,
        attention,
        emb_dim,
        output_dim,
        enc_hid_dim,
        dec_hid_dim,
        dec_layers,
        dropout=0.1,
    ):
        super().__init__()

        self.output_dim = output_dim
        self.attention = attention

        self.rnn = nn.GRU(
            enc_hid_dim + emb_dim, dec_hid_dim, num_layers=dec_layers, dropout=dropout
        )

        self.fc_out = nn.Linear(enc_hid_dim + dec_hid_dim + emb_dim, output_dim)

        self.dropout = nn.Dropout(dropout)

    def forward(self, emb, hidden, encoder_outputs):
        # input = [batch size]
        # hidden = [batch size, dec hid dim]
        # encoder_outputs = [batch size, src len, enc hid dim]

        # input = [1, batch size]

        embedded = self.dropout(emb)

        # embedded = [1, batch size, emb dim]

        a = self.attention(hidden[-1], encoder_outputs)

        # a = [batch size, src len]

        a = a.unsqueeze(1)

        # a = [batch size, 1, src len]

        weighted = torch.bmm(a, encoder_outputs)

        # weighted = [batch size, 1, enc hid dim * 2]

        weighted = weighted.permute(1, 0, 2)

        # weighted = [1, batch size, enc hid dim * 2]

        rnn_input = torch.cat((embedded, weighted), dim=2)

        # rnn_input = [1, batch size, (enc hid dim * 2) + emb dim]

        output, hidden = self.rnn(rnn_input, hidden)

        # output = [seq len, batch size, dec hid dim * n directions]
        # hidden = [n layers * n directions, batch size, dec hid dim]

        # seq len, n layers and n directions will always be 1 in this decoder, therefore:
        # output = [1, batch size, dec hid dim]
        # hidden = [1, batch size, dec hid dim]
        # this also means that output == hidden
        #         assert (output == hidden).all()

        embedded = embedded.squeeze(0)
        output = output.squeeze(0)
        weighted = weighted.squeeze(0)

        prediction = self.fc_out(torch.cat((output, weighted, embedded), dim=1))

        # prediction = [batch size, output dim]

        return prediction, hidden.squeeze(0)


class BERT2LSTM(nn.Module):
    def __init__(
        self,
        emb_num,
        dec_hid_dim=768,
        dec_layers=2,
        dec_dropout=0.1,
        pretrain="bert-base-uncased",
    ):
        super(BERT2LSTM, self).__init__()
        self.bert = BertModel.from_pretrained(pretrain)
        self.bert.resize_token_embeddings(emb_num)
        self.dec_layers = dec_layers
        bert_hid_dim = BertConfig.from_pretrained(pretrain).hidden_size
        attention = Attention(bert_hid_dim, dec_hid_dim)
        self.decoder = Decoder(
            attention,
            bert_hid_dim,
            emb_num,
            bert_hid_dim,
            dec_hid_dim,
            dec_layers,
            dec_dropout,
        )

    def forward(self, src, trg, teacher_forcing_ratio=0.5):
        # src = [batch size, src len]
        # trg = [batch size, trg len]
        # teacher_forcing_ratio is probability to use teacher forcing
        # e.g. if teacher_forcing_ratio is 0.75 we use teacher forcing 75% of the time

        batch_size = src.shape[0]
        trg_len = trg.shape[1]
        trg_vocab_size = self.decoder.output_dim

        # tensor to store decoder outputs
        outputs = torch.zeros(
            batch_size, trg_len, trg_vocab_size, device=src.get_device()
        )

        # encoder_outputs is all hidden states of the input sequence, back and forwards
        # hidden is the final forward and backward hidden states, passed through a linear layer
        encoder_outputs = self.bert(src)[0]
        hidden = torch.zeros(
            self.dec_layers,
            batch_size,
            encoder_outputs.size(2),
            device=src.get_device(),
        )

        # first input to the decoder is the <sos> tokens
        input_ = trg[:, 0]

        for t in range(1, trg_len):
            # insert input token embedding, previous hidden state and all encoder hidden states
            # receive output tensor (predictions) and new hidden state
            embeddings = self.bert.embeddings(input_.unsqueeze(0))
            output, hidden = self.decoder(embeddings, hidden, encoder_outputs)

            # place predictions in a tensor holding predictions for each token
            outputs[:, t] = output

            # decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio

            # get the highest predicted token from our predictions
            top1 = output.argmax(1)

            # if teacher forcing, use actual next token as next input
            # if not, use predicted token
            input_ = trg[:, t] if teacher_force else top1

        return outputs
