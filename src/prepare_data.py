import pandas as ps
from transformers import BertTokenizer, BertTokenizerFast
from tqdm import tqdm

# local files
import config


def main():
    tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
    num_added_tokens = tokenizer.add_tokens(
        [config.SOS, config.EOS], special_tokens=True
    )
    print(f"Aded {num_added_tokens} tokens.")
    tokenizer.save_pretrained(str(config.TOKENIZER_DIR))

    tokenizer = BertTokenizerFast.from_pretrained(str(config.TOKENIZER_DIR))

    print("To token ids:", tokenizer.convert_tokens_to_ids([config.SOS, config.EOS]))
    print(
        "From token ids:",
        tokenizer.convert_ids_to_tokens(
            tokenizer.convert_tokens_to_ids([config.SOS, config.EOS])
        ),
    )

    def abstract_to_ids(text):
        tokens = tokenizer.tokenize(text)
        return tokenizer.convert_tokens_to_ids(tokens)

    def title_to_ids(text):
        tokens = [config.SOS] + tokenizer.tokenize(text) + [config.EOS]
        return tokenizer.convert_tokens_to_ids(tokens)

    data = ps.read_csv(config.TRAIN_CSV)
    print(f"Train data shapes - {data.shape}")

    data["abstract_tokens"] = [
        abstract_to_ids(text) for text in tqdm(data["abstract"].values, desc="abstract")
    ]
    data["title_tokens"] = [
        title_to_ids(text) for text in tqdm(data["title"].values, desc="title")
    ]

    data.to_pickle(config.TRAIN_TOKENIZED)
    print(f"Saved results to {config.TRAIN_TOKENIZED}")


if __name__ == "__main__":
    main()
